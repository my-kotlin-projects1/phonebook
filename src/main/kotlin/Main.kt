fun main() {
    // лист номеров и работа с ними.
    val listNumbers = userInput()
    println("Изначальный список $listNumbers")
    filteredOutput(listNumbers)
    val listNumbersUnic = listNumbers.toSet().toList()
    println("Уникальных записей в книге: ${listNumbersUnic.size}")
    val sum = listNumbers.sumOf { it.length }
    println("Сумма длинн всех номеров в записях: $sum")

    // лист имен для номеров
    val listNames = mutableListOf<String>()
    listNumbersUnic.forEach {
        println("Введите имя контакта для номера $it")
        listNames.add(readln())
    }

    // коллекция контактов и вывод в консоль
    val contacts = mutableMapOf<String, String>()
    for (number in listNumbersUnic){
        contacts[number]=listNames[listNumbersUnic.indexOf(number)]
    }
    contacts.forEach { entry -> println("Человек: ${entry.value}. Номер телефона: ${entry.key}")  }
}

fun filteredOutput(list: MutableList<String>){
    println("Список номеров начинающихся с +7 ${list.filter { it.contains("+7")}}")
}

fun userInput(): MutableList<String> {
    val listNumbers = mutableListOf<String>()
    println("Введите целое положительное число, количество контактов в книге")

    try {
        var phoneNumbersCount = readln().toInt()
        while (phoneNumbersCount <= 0) {
            println("Введите целое положительное число, количество контактов в книге")
            phoneNumbersCount = readln().toInt()
        }
        repeat(phoneNumbersCount){
            println("Введите номер телефона для контакта ${it + 1}")
            val contactNumber = readln()
            listNumbers.add(contactNumber)
        }
    } catch (e: NumberFormatException) {
        userInput()
    }
    return listNumbers
}
